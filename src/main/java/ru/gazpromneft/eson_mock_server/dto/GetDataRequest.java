
package ru.gazpromneft.eson_mock_server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inRecipient" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="inoutMessageNo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="inReceivedNoMessageFromBus" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="inErrorsOfReceivedMessageFromBus" type="{http://www.gazpromneft.ru/eson_esaul_mc_integration}ListOfErrors"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inRecipient",
    "inoutMessageNo",
    "inReceivedNoMessageFromBus",
    "inErrorsOfReceivedMessageFromBus"
})
@XmlRootElement(name = "getDataRequest", namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration")
@ToString
@Getter
@Setter
public class GetDataRequest {

    @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
    protected String inRecipient;
    @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
    protected BigInteger inoutMessageNo;
    @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
    protected BigInteger inReceivedNoMessageFromBus;
    @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
    protected ListOfErrors inErrorsOfReceivedMessageFromBus;

}

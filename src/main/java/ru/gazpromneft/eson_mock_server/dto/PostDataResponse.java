
package ru.gazpromneft.eson_mock_server.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inoutMessageNo" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="outReceivedNoMessageFromIS" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="outErrorsOfReceivedMessageFromIS" type="{http://www.gazpromneft.ru/eson_esaul_mc_integration}ListOfErrors"/>
 *         &lt;element name="outErrors" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "inoutMessageNo",
    "outReceivedNoMessageFromIS",
    "outErrorsOfReceivedMessageFromIS",
    "outErrors"
})
@XmlRootElement(name = "postDataResponse", namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration")
@ToString
@Getter
@Setter
public class PostDataResponse {

  @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
  protected BigInteger inoutMessageNo;
  @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
  protected BigInteger outReceivedNoMessageFromIS;
  @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
  protected ListOfErrors outErrorsOfReceivedMessageFromIS;
  @XmlElement(namespace = "http://www.gazpromneft.ru/eson_esaul_mc_integration", required = true)
  protected String outErrors;

}

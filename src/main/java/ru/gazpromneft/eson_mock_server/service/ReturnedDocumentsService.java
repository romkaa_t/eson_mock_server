package ru.gazpromneft.eson_mock_server.service;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import ru.gazpromneft.eson_mock_server.application.ResponseSettings;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

@Service
public class ReturnedDocumentsService {
  @Autowired
  private ResponseSettings settings;

  public String mockPayload() {
    Resource r = new ClassPathResource(
        String.format("payloads/%s/%s.xml",
            String.valueOf(settings.getRecipientType()).toLowerCase(),
            String.valueOf(settings.getReportType()).toLowerCase()));
    return getResourceAsString(r);
  }

  private String getResourceAsString(Resource resource) {
    try (InputStream stream = resource.getInputStream()) {
      return IOUtils.toString(stream, StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new RuntimeException(String.format("Could not open document for recipient %s and type %s",
          settings.getRecipientType(),
          settings.getReportType()));
    }
  }

  public enum RecipientType {
    ESAUL, MC
  }

  public enum DocumentType {
    DAILY_PLAN, DAILY_PLAN_UNCONFIRMED, DAILY_QUOTAS, SHIPPING_ORDER, STATISTICAL_LOAD, WAYBILL, NO_DATA
  }

}

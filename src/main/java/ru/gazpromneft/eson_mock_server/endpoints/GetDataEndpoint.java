package ru.gazpromneft.eson_mock_server.endpoints;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.gazpromneft.eson_mock_server.dto.GetDataRequest;
import ru.gazpromneft.eson_mock_server.dto.GetDataResponse;
import ru.gazpromneft.eson_mock_server.service.ReturnedDocumentsService;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.time.Instant;

@Endpoint
@Slf4j
public class GetDataEndpoint {
  @Autowired
  ReturnedDocumentsService payloadMockService;
  private static final String NAMESPACE_URI = "http://www.gazpromneft.ru/eson_esaul_mc_integration";

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getDataRequest")
  @ResponsePayload
  public GetDataResponse getData(@RequestPayload GetDataRequest request) throws DatatypeConfigurationException {

    log.info("Request for GetDataResponse: " + request);

    GetDataResponse response = new GetDataResponse();

    response.setOutData(payloadMockService.mockPayload());

    String dateTimeString = Instant.now().toString();
    XMLGregorianCalendar date = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateTimeString);
    System.out.println(date);
    response.setOutMessageReadyDate(date);

    response.setOutObjectsQuantity(BigInteger.valueOf(123L));
    response.setOutReceivedNoMessageFromIS(BigInteger.valueOf(321L));


    log.info("Response for GetDataResponse: " + response);
    return response;

  }
}

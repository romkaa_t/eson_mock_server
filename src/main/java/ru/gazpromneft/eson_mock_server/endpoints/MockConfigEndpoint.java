package ru.gazpromneft.eson_mock_server.endpoints;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.gazpromneft.eson_mock_server.application.ResponseSettings;

import java.util.concurrent.atomic.AtomicBoolean;

@RestController
public class MockConfigEndpoint {
  @Autowired
  private
  ResponseSettings responseSettings;

  private AtomicBoolean b = new AtomicBoolean(false);

  @PostMapping("/nextResponse")
  public void patchResponseConfig(@RequestBody ResponseSettings responseSettings) {
    BeanUtils.copyProperties(responseSettings, this.responseSettings);
  }

  @GetMapping("/error")
  public ResponseEntity<String> throwError() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("error","error");
    return getStringResponseEntity(403, headers);
  }

  private ResponseEntity<String> getStringResponseEntity(int status, HttpHeaders headers) {
    return ResponseEntity.status(status)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(headers)
        .body("{\"test\":\"test\"}");
  }

  @GetMapping("/401")
  public ResponseEntity<String> returnOk() {
    HttpHeaders headers = new HttpHeaders();
    headers.set("www-authenticate",b.get()?"NTLM":"TlRMTVNTUAACAAAADAAMADgAAAAGgokCe3WY1bReCT4AAAAAAAAAAOYA5gBEAAAABgOAJQAAAA9HQVpQUk9NLU5FRlQCABgARwBBAFoAUABSAE8ATQAtAE4ARQBGAFQAAQAeAFMAUABCADkAOQAtAFQALQBHAFAATgBBAFAAUAAxAAQAJABnAGEAegBwAHIAbwBtAC0AbgBlAGYAdAAuAGwAbwBjAGEAbAADAEQAcwBwAGIAOQA5AC0AdAAtAGcAcABuAGEAcABwADEALgBnAGEAegBwAHIAbwBtAC0AbgBlAGYAdAAuAGwAbwBjAGEAbAAFACQAZwBhAHoAcAByAG8AbQAtAG4AZQBmAHQALgBsAG8AYwBhAGwABwAIAJw7i3On1tUBAAAAAA");
    b.set(!b.get());
    return getStringResponseEntity(401, headers);
  }

}

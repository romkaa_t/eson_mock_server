package ru.gazpromneft.eson_mock_server.endpoints;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.gazpromneft.eson_mock_server.dto.PostDataRequest;
import ru.gazpromneft.eson_mock_server.dto.PostDataResponse;

import java.math.BigInteger;

@Endpoint
@Slf4j
public class PostDataEndpoint {

  private static final String NAMESPACE_URI = "http://www.gazpromneft.ru/eson_esaul_mc_integration";

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "postDataRequest")
  @ResponsePayload
  public PostDataResponse postData(@RequestPayload PostDataRequest request) {

    log.info("Request: " + request);

    PostDataResponse response = new PostDataResponse();

    response.setInoutMessageNo(BigInteger.valueOf(123L));
    response.setOutReceivedNoMessageFromIS(BigInteger.valueOf(123));

    log.info("Response for PostDataResponse: " + response);
    return response;

  }

}

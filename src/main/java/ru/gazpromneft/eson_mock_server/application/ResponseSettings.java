package ru.gazpromneft.eson_mock_server.application;

import lombok.Data;
import org.springframework.stereotype.Component;
import ru.gazpromneft.eson_mock_server.service.ReturnedDocumentsService;

import java.util.List;

@Data
@Component
public class ResponseSettings {
  private ReturnedDocumentsService.RecipientType recipientType;
  private ReturnedDocumentsService.DocumentType reportType;
  private List<String> errors;
}

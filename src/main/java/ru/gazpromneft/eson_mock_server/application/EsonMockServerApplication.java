package ru.gazpromneft.eson_mock_server.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "ru.gazpromneft.eson_mock_server")
public class EsonMockServerApplication {

  public static void main(String[] args) {
    SpringApplication.run(EsonMockServerApplication.class, args);
  }

}
